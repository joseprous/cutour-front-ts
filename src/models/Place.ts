export type Place = {
  id: number;
  image: string;
  title: string;
  description: string;
};
