export type Category = {
  id: number;
  label: string;
  icon: string;
};
